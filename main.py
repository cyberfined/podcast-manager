import pygtk
pygtk.require('2.0')
import gtk
from dialogs import *
import parser
import resource
import utils
import cache

config = "pod.conf"
resources=dict()
podcasts = []
downloads =[]

try:
    resources = resource.read_config(config)
except IOError as error:
    print str(error)
except ValueError as error:
    print("Config Error", str(error))
    exit()
except Exception as error:
    print str(error)
    exit()

gtk.gdk.threads_init()

#Window
window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.set_title("Podcasts")
window.set_default_size(500, 500)
window.set_border_width(10)
window.set_position(gtk.WIN_POS_CENTER)
window.connect("destroy", gtk.main_quit)

#Menu
menu = gtk.MenuBar()
edit_item = gtk.MenuItem("Edit Podcasts")
update_item = gtk.MenuItem("Update Cache")
quit_item = gtk.MenuItem("Quit")
menu.append(edit_item)
menu.append(update_item)
menu.append(quit_item)

def edit_item_cb(event, item):
    global resources
    srd = show_resources_dialog.ShowResDialog(resources)
    if srd.run() == gtk.RESPONSE_OK:
        resources = srd.resources
        utils.combobox_load_resources(combobox, resources)
        model.clear()
        resource.write_config(resources, config)
    srd.destroy()

def update_item_cb(event, item):
    try:
        cache.cache_all_update(resources)
        msg = gtk.MessageDialog(type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_YES_NO, message_format="Update Complete!")
        msg.run()
        msg.destroy()
    except IOError as error:
        print str(error)
    except ValueError as error:
        print str(error)

edit_item.connect("button_press_event", edit_item_cb)
update_item.connect("button_press_event", update_item_cb)
quit_item.connect("button_press_event", gtk.main_quit)

#TreeView
model = gtk.ListStore(str)
tree = gtk.TreeView(model)
tree.get_selection().set_mode(gtk.SELECTION_MULTIPLE)
tree.append_column(gtk.TreeViewColumn("Podcast", gtk.CellRendererText(), text=0))
swin = gtk.ScrolledWindow()
swin.add(tree)

def tree_foreach(treemodel, path, treeiter):
    downloads.append(int(path[0]))

#Combobox
combobox = gtk.combo_box_new_text()
utils.combobox_load_resources(combobox, resources)

def combobox_cb(widget):
    cm = widget.get_model()
    index = widget.get_active()
    if index == -1:
        return
    global podcasts
    try:
        rss_text = cache.get_rss_string(resources[cm[index][0]])
        podcasts = parser.parse_rss(rss_text)
        utils.list_append_podcasts(tree, podcasts)
    except IOError as error: #urllib.urlopen, open
        print str(error)
    except ValueError as error: #parser.strcpy, strcut
        print str(error)

combobox.connect("changed", combobox_cb)

#Download Button
download_button = gtk.Button("Download")

def download_button_cb(event, button):
    global podcasts
    global downloads
    downloads=[]
    chooser = gtk.FileChooserDialog(title="Select A Folder", action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER, buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    if chooser.run() == gtk.RESPONSE_OK:
        savepath = chooser.get_filename() + "/"
        tree.get_selection().selected_foreach(tree_foreach)
        chooser.destroy()
        dd = downloads_dialog.DownloadsDialog(podcasts, downloads, savepath)
        dd.run()
        dd.destroy()
        return
    chooser.destroy()

download_button.connect("button_press_event", download_button_cb)

#VBox
vbox = gtk.VBox(0)
vbox.pack_start(menu, False, False, 0)
vbox.pack_start(combobox, False, False, 0)
vbox.pack_start(swin, True, True, 0)
vbox.pack_start(download_button, False, False, 0)

window.add(vbox)
window.show_all();

gtk.main()
