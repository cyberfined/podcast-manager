import urllib
import os
import parser
import resource
cachedir = "cache/"

def cache_compress(cache):
    com_cache = ""
    while "<item>" in cache:
        com_cache += "<item>" + parser.strcpy_se(cache, "<item>", "</item>") + "</item>"
        cache = parser.strcut_se(cache, "<item>", "</item>")
    return com_cache

def cache_resource_update(res):
    if not os.path.exists(cachedir):
        os.makedirs(cachedir)
    conn = urllib.urlopen(res.preferences["url"])
    data = conn.read()
    data = cache_compress(data)
    cachefile = open(cachedir + res.preferences["cache"], "w")
    cachefile.write(data)
    cachefile.close()

def cache_all_update(resources):
    for k in resources:
        cache_resource_update(resources[k])

def get_rss_string(res):
    if not os.path.exists(cachedir + res.preferences["cache"]):
        cache_resource_update(res)

    cachefile = open(cachedir + res.preferences["cache"], "r")
    data = cachefile.read()
    cachefile.close()

    return data
