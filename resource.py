import parser

class Resource:
    def __init__(self):
        self.title = ""
        self.preferences = dict()
    def add_to_conf(self, filename):
        config = open(filename, "a")
        config.write(self.title + "{")
        for k in self.preferences:
            config.write("\n\t" + k + "==" + self.preferences[k] + ";")
        config.write("\n}\n")
        config.close()
    def parse_str(self, string):
        self.title = parser.strcpy_before(string, "{").strip()
        lines = parser.strcpy_se(string, "{", "}").split(';')
        if len(lines) == 0:raise Exception("Config Error")
        for line in lines:
            line = line.strip()
            if line == "":break
            tokens = line.split('==')
            if len(tokens) != 2:
                raise Exception("Config Error!")
            self.preferences[tokens[0]] = tokens[1]
        return parser.strcut_se(string, self.title, "}")

def write_config(resources, filename):
    config_file = open(filename, "w")

    for res in resources:
        config_file.write(resources[res].title + "{")
        for prop in resources[res].preferences:
            config_file.write("\n\t" + prop + "==" + resources[res].preferences[prop] + ";")
        config_file.write("\n}\n")

    config_file.close()

def read_config(filename):
    config_file = open(filename, "r")
    data = config_file.read()
    resources = dict()

    while "{" in data:
        resource = Resource()
        data = resource.parse_str(data)
        resources[resource.title] = resource
    config_file.close()
    return resources
