import pygtk
pygtk.require('2.0')
import gtk
import resource

class AddResDialog(gtk.Dialog):
    def __init__(self, **args):
        gtk.Dialog.__init__(self, title="Add Podcast RSS", buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))
        self.entries = dict()

        self.add_entry_label("title", "Title")
        self.add_entry_label("url", "URL")
        self.add_entry_label("cache", "Cachefile")

        for k in args:
            self.entries[k].set_text(args[k])

    def is_valid(self):
        for k in self.entries:
            if self.entries[k].get_text().strip() == "":
                return False
        return True
    def get_resource(self):
        res = resource.Resource()
        res.title = self.entries["title"].get_text()
        for k in self.entries:
            if k == "title":continue
            res.preferences[k] = self.entries[k].get_text()
        return res
    def add_entry_label(self, key, text):
        vbox = self.get_content_area()
        hbox = gtk.HBox(0)
        entry = gtk.Entry()
        self.entries[key] = entry
        hbox.pack_start(gtk.Label(text), False, False, 0)
        hbox.pack_start(entry, True, True, 0)
        hbox.show_all()
        vbox.pack_start(hbox, True, True, 0)
