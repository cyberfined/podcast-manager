import pygtk
pygtk.require('2.0')
import gtk
import resource
import add_resource_dialog

class ShowResDialog(gtk.Dialog):
    def __init__(self, resources):
        gtk.Dialog.__init__(self, title="Edit Resources", buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))
        self.copy_resources(resources)
        self.box = self.get_content_area()

        #TreeView
        self.model = gtk.ListStore(str, str, str)
        self.tree = gtk.TreeView(self.model)
        self.tree.append_column(gtk.TreeViewColumn("Resource", gtk.CellRendererText(), text=0))
        self.tree.append_column(gtk.TreeViewColumn("URL", gtk.CellRendererText(), text=1))
        self.tree.append_column(gtk.TreeViewColumn("Cachefile", gtk.CellRendererText(), text=2))
        swin = gtk.ScrolledWindow()
        swin.add(self.tree)

        for k in self.resources:
            self.model.append([self.resources[k].title, self.resources[k].preferences["url"], self.resources[k].preferences["cache"]])

        self.box.pack_start(swin, True, True, 0)

        #Hbox
        hbox = gtk.HBox(0)

        #Delete Button
        button = gtk.Button("Delete")
        button.connect("button_press_event", self.delete_button_cb)
        hbox.pack_start(button, True, True, 0)

        #Edit Button
        button = gtk.Button("Edit")
        button.connect("button_press_event", self.edit_button_cb)
        hbox.pack_start(button, True, True, 0)

        #Append Button
        button = gtk.Button("Append")
        button.connect("button_press_event", self.append_button_cb)
        hbox.pack_start(button, True, True, 0)

        self.box.pack_start(hbox, False, False, 0)
        self.box.show_all()
        self.set_size_request(600, 200)
    def copy_resources(self, resources):
        self.resources = dict()
        for k in resources:
            self.resources[k] = resources[k]
    def delete_button_cb(self, event, button):
        treeiter = self.tree.get_selection().get_selected()
        if treeiter[1] == None:return
        key = self.model[treeiter[1]][0]
        del(self.resources[key])
        self.model.remove(treeiter[1])
    def edit_button_cb(self, event, button):
        treeiter = self.tree.get_selection().get_selected()
        if treeiter[1] == None:return
        title = self.model[treeiter[1]][0]
        url = self.model[treeiter[1]][1]
        cache = self.model[treeiter[1]][2]

        ard = add_resource_dialog.AddResDialog(title=title, url=url, cache=cache)
        if ard.run() == gtk.RESPONSE_OK:
            if not ard.is_valid():
                md = gtk.MessageDialog(type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_YES_NO, message_format="All Fields Must Be filled!")
                md.run()
                md.destroy()
                ard.destroy()
                return

            res = ard.get_resource()
            del(self.resources[title])
            self.resources[res.title] = res

            self.model.set_value(treeiter[1], 0, res.title)
            self.model.set_value(treeiter[1], 1, res.preferences["url"])
            self.model.set_value(treeiter[1], 2, res.preferences["cache"])
        ard.destroy()
    def append_button_cb(self, event, button):
        ard = add_resource_dialog.AddResDialog()
        if ard.run() == gtk.RESPONSE_OK:
            if not ard.is_valid():
                md = gtk.MessageDialog(type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_YES_NO, message_format="All Fields Must Be filled!")
                md.run()
                md.destroy()
                ard.destroy()
                return

            res = ard.get_resource()
            self.resources[res.title] = res
            self.model.append([res.title, res.preferences["url"], res.preferences["cache"]])
        ard.destroy()
