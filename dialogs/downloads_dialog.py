import pygtk
pygtk.require('2.0')
import gtk
import urllib
import threading

class DownloadsDialog(gtk.Dialog):
    def __init__(self, podcasts, indexes, savepath):
        gtk.Dialog.__init__(self, title="Downloads", buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL))
        self.prog_bars=[]
        self.podcasts=[]
        self.downloaders=[]
        self.savepath = savepath
        self.count = len(indexes)
        box = self.get_content_area()

        for i in indexes:
            self.podcasts.append(podcasts[i])
            self.prog_bars.append(gtk.ProgressBar())
            bar = len(self.prog_bars)-1
            self.prog_bars[bar].show()
            self.prog_bars[bar].set_text(podcasts[i].title)
            box.pack_start(self.prog_bars[bar], True, True, 0)
        self.download_start()

    def complete(self):
        self.count -= 1
        if self.count == 0:
            self.response(gtk.RESPONSE_OK)

    def download_start(self):
        for i in range(len(self.podcasts)):
            d = Downloader(self.podcasts[i], self.savepath, self.prog_bars[i], self.complete)
            d.start()
            self.downloaders.append(d)

    def destroy(self):
        for d in self.downloaders:
            d.stop()
            del(d)
        gtk.Dialog.destroy(self)

class Downloader(threading.Thread):
    isRun = True

    def __init__(self, podcast, savepath, progbar, callback):
        threading.Thread.__init__(self)
        self.podcast = podcast
        self.savepath = savepath
        self.progbar = progbar
        self.callback = callback

    def run(self):
        conn = urllib.urlopen(self.podcast.url)
        size = float(conn.info().getheader("Content-Length"))
        cursize = float(0)
        savefile = open(self.savepath + self.podcast.title + ".mp3", "w")

        while self.isRun:
            data = conn.read(4096)
            if not data: break
            savefile.write(data)
            cursize += len(data)
            gtk.threads_enter()
            self.progbar.set_fraction(cursize/size)
            gtk.threads_leave()

        self.callback()
        savefile.close()

    def stop(self):
        self.isRun = False
