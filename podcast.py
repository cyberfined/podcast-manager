class Podcast:
    def __init__(self, title, description, url):
        self.title = title
        self.description = description
        self.url = url
    def __str__(self):
        return self.title+"\n"+self.description+"\n"+self.url+"\n"
