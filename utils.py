import pygtk
pygtk.require('2.0')
import gtk
import podcast

def list_append_podcasts(treeview, podcasts):
    model = treeview.get_model()
    model.clear()
    for p in podcasts:
        model.append([str(p)])

def combobox_load_resources(combobox, resources):
    combobox.get_model().clear()
    for k in resources:
        combobox.append_text(k)
