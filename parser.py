import podcast

def strcpy_se(string, start, end):
    s = string.index(start) + len(start)
    e = string[s:].index(end) + s
    return string[s:e]

def strcut_se(string, start, end):
    s = string.index(start) + len(start)
    e = string[s:].index(end) + s + len(end)
    return string[e:]

def strcpy_before(string, delim):
    index = string.index(delim)
    return string[:index]

def parse_item(item):
    title = strcpy_se(item, "<title>", "</title>")
    description = strcpy_se(item, "<description>", "</description>")
    url = strcpy_se(item, "<enclosure", "/>")
    url = strcpy_se(url, "url=\"", "\"")
    return podcast.Podcast(title, description, url)

def parse_rss(rss_text):
    podcasts = []
    item = ""

    while "<item>" in rss_text:
        item = strcpy_se(rss_text, "<item>", "</item>")
        rss_text = strcut_se(rss_text, "<item>", "</item>")
        podcasts.append(parse_item(item))
    return podcasts
